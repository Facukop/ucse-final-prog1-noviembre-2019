﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logica;

namespace Logica
{
    class PiletaLona:PiletasComercializadas
    {
        public double Alto { get; set; }
        public double Ancho { get; set; }
        public double Profundidad { get; set; }
        public bool VieneConFiltroONo { get; set; }
        public bool VieneConCubrePiletasONo { get; set; }

        public override int CalcularPorcentajeDeDescuento()
        {
            return 8;
        }
    } 
}
