﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logica;

namespace Logica
{
    public class Empresa
    {
        List<Usuario> ListaUsuarios = new List<Usuario>();
        List<Ventas> ListaVentas = new List<Ventas>();
        List<PiletasComercializadas> ListaPiletas = new List<PiletasComercializadas>();
        
        public bool RegistrarCompra(int DNIcomprador, int CODpileta)
        {
            bool resultado = false;
            string mensaje = "";
            double importeFinal = 0;

            var usuario = ListaUsuarios.Find(user => user.DNI == DNIcomprador);

            var pileta = ListaPiletas.Find(pile => pile.CodigoPileta == CODpileta);
            if (usuario != null)
            {
                if (usuario is Empleado)
                {

                    int descuentoPileta = pileta.CalcularPorcentajeDeDescuento();

                    if (descuentoPileta != 0)
                    {
                        double MontoARestar = (descuentoPileta * pileta.PrecioPileta) / 100;
                        importeFinal = pileta.PrecioPileta - MontoARestar;

                     
                    }
                    else
                    {
                        importeFinal = pileta.PrecioPileta;
                    }

                    mensaje = "El registro se realizo correctamente";

                }
                else
                {
                    Cliente cliente = usuario as Cliente;
                    cliente.FechaDeInstalacion = DateTime.Now;
                    cliente.CodigoPiletaInstalada = pileta.CodigoPileta;
                    importeFinal = pileta.PrecioPileta;

                    mensaje = "Se registro la nueva pileta del cliente";
                }

                resultado = true;


            }
            else
            {
                importeFinal = pileta.PrecioPileta;
                //CORRECCIÓN: Deberías crear un cliente, no un empleado. De esta manera te evitas tener que crear el método CargarNuevoCliente, que de paso está mal para le modelado.
                Empleado empleado = new Empleado();

                var cliente = empleado.CargarNuevoCliente(DNIcomprador, pileta.CodigoPileta);

                ListaUsuarios.Add(cliente);

                mensaje = "Se dio de alta la venta y el cliente, recuerde que debe completar sus datos";
                resultado = true;

                
                
            }

            Ventas nuevaVenta = new Ventas(DNIcomprador, importeFinal);
            ListaVentas.Add(nuevaVenta);

            //CORRECCIÓN: Esto está mal, además no lo guardas en ningún lado.
            //La idea de esto es que tengas una clase de bool y string, y que el método devuelva esta clase como resultado.
            RegistrarCompra(mensaje);
            return resultado;
        }
//CORRECCIÓN: Esto no tiene una finalidad válida.
        public string RegistrarCompra(string mensaje)
        {
            return mensaje;
        }
    }
}
