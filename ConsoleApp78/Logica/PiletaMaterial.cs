﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logica;

namespace Logica
{
    class PiletaMaterial:PiletasComercializadas
    {
      
        public double Largo { get; set; }
        public double Profundidad { get; set; }
        public double Ancho { get; set; }
        public bool TieneTrampolinONo { get; set; }
        public int CantidadEscaleras { get; set; }

        public override int CalcularPorcentajeDeDescuento()
        {
            int descuento = 0;
            if ((Profundidad > 1.5)&&(TieneTrampolinONo == true))
            {
                descuento = 10;
            }
            else
            {
                descuento = 5;
            }

            return descuento;
        }
    } 
}
