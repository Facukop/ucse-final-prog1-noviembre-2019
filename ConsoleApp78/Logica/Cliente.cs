﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logica;

namespace Logica
{
    class Cliente: Usuario
    {
        public int CodigoPiletaInstalada { get; set; }
        public DateTime FechaDeInstalacion { get; set; }

        public Cliente(int dni, int codPileta)
        {
            FechaDeInstalacion = DateTime.Now;
            CodigoPiletaInstalada = codPileta;
            DNI = dni;
        }

        public override string DevolverDetalleUsuario()
        {
            return base.DevolverDetalleUsuario();
        }

        public override string DevolverMensaje()
        {
            int cantDias = (DateTime.Now - FechaDeInstalacion).Days;

            string mensaje = "";

//CORRECCIÓN: La condición está mal, luego de que se cumpla el primer día de aniversario, 
//los restantes días siempre el valor va a ser mayor a 365 y devolvería siempre el mimso 
//mensaje, peor ya no seria mas el primer aniversario.
            if (cantDias >= 365)
            {
                mensaje = "Felicitaciones por su primer aniversario de instalacion!";
            }

            return mensaje;
        }
    }
}
