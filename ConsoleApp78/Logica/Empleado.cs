﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logica;

namespace Logica
{
    class Empleado:Usuario
    {
        public DateTime FechaNacimiento { get; set; }
        public string TurnoTrabaja { get; set; }
        public string AreaDesempeña { get; set; }

//CORRECCIÓN: Se debía usar un enumerador para estos valores
        public Empleado(int turno, int area)
        {
            if (turno == 0)
            {
                TurnoTrabaja = "Mañana";
            }
            else
            {
                if (turno == 1)
                {
                    TurnoTrabaja = "Tarde";
                }
            }

            switch (area)
            {
                case 0:
                    AreaDesempeña = "Gerencia";
                    break;
                case 1:
                    AreaDesempeña = "Ventas";
                    break;
                case 2:
                    AreaDesempeña = "Instalacion";
                    break;

                default:
                    break;
            }
        }
        public Empleado()
        {

        }

        public override string DevolverDetalleUsuario()
        {
            return base.DevolverDetalleUsuario() + " - " + "Turno: " + TurnoTrabaja + " - " + "Area: " + AreaDesempeña;
        }

        public override string DevolverMensaje()
        {
            string mensaje = "";

            //CORRECCION: La fecha de nacimiento nunca va a ser igual a la fecha de hoy.
            if (FechaNacimiento == DateTime.Now)
            {
                mensaje = "Feliz cumpleaños compañero!";

            }

            return mensaje;
        }

        public Cliente CargarNuevoCliente(int dniClien, int codPile)
        {
            Cliente nuevoCliente = new Cliente(dniClien, codPile);

            return nuevoCliente;
        }


    } 
}
