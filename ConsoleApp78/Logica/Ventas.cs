﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logica;

namespace Logica
{
    class Ventas
    {
        public int NumeroDeVenta { get; set; }
        public int DNIcomprador { get; set; }
        public DateTime FechaVenta { get; set; }
        public double ImporteTotal { get; set; }
        
        public Ventas(int dniComprador, double importe)
        {
            //CORRECCIÓN: Esto lo explicamos muchas veces de que no sirve, se necesita sacar el length de la lista para hacer el autoincremental
            NumeroDeVenta += 1;
            FechaVenta = DateTime.Now;
            DNIcomprador = dniComprador;
            ImporteTotal = importe;
        }

    }
}
