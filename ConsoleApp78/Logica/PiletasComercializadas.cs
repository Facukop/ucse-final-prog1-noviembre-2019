﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logica;

namespace Logica
{
   abstract class PiletasComercializadas
    {
        public int CodigoPileta { get; set; }
        public double CantidadLitrosPileta { get; set; }
        public double PrecioPileta { get; set; }
        public string Color { get; set; }
         
        public PiletasComercializadas()
        {

        }

//CORRECCIÓN: Se debía usar un enumerador para estos valores
        public PiletasComercializadas(int color)
        {
            switch (color)
            {
                case 0:
                    Color = "Azul";
                    break;
                case 1:
                    Color = "Celeste";
                    break;
                case 2:
                    Color = "Gris";
                    break;

                default:
                    break;
            }
        }

        public abstract int CalcularPorcentajeDeDescuento();
    }
}
