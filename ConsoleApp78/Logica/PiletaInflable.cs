﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logica;

namespace Logica
{
    class PiletaInflable:PiletasComercializadas
    {
        public double Diametro { get; set; }
        public double Profundidad { get; set; }
        public bool VieneConCubrePiletaONo { get; set; }


        public override int CalcularPorcentajeDeDescuento()
        {
            return 0;
        }
    } 
}
