﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logica;

namespace Logica
{
    abstract class Usuario
    {
        public int DNI { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Telefono { get; set; }
        public string Direccion { get; set; }
        public Localidad Localidad { get; set; }
         
        public virtual string DevolverDetalleUsuario()
        {
            string tel = Telefono.ToString();

            return Apellido + ", " + Nombre + " - " + "Telefono: " + tel + " - " + Localidad.Provincia + ", " +
                Localidad.NombreLocalidad + ", " + Direccion;


        }

        public abstract string DevolverMensaje();

    }
}
